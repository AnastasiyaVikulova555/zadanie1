unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure Drawlines(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
CONST NUM_LINES = 800000;

{$R *.dfm}


procedure TForm1.FormCreate(Sender: TObject);
begin
Randomize;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
Drawlines(Sender);
end;

procedure TForm1.Drawlines(Sender: TObject);
var i:integer;
begin
  for i := 0 to NUM_LINES - 1 do
  begin
    Canvas.Pen.Color :=
    RGB(Random(256),Random(256),Random(256));
    Canvas.LineTo
    (Random(ClientWidth),Random(ClientHeight));
  end;


end;
end.
